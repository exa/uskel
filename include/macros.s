
.ifndef _macros_s_file
_macros_s_file:

.macro enter_rbp
	jmp *(%rbp)
.endm

.macro enter x
	mov \x, %rbp
	enter_rbp
.endm

.macro cell x
	.quad \x
.endm

.macro continue
	xchg %rsi, %rbp
	enter_rbp
.endm

.macro thunkenv arg, dest
	mov  (8*(\arg+2))(%rbp), \dest
.endm

.macro enter_thunkenv arg
	mov  (8*(\arg+2))(%rbp), %rbp
	jmp *(%rbp)
.endm

# this needs data.s
.macro .thunkcode name
	.align 8
	__\name\()_info:
		cell THU_evacuate
		cell THU_scavenge
		cell 0
	\name:
.endm

.macro thunk arg,args:vararg
	.ifnb \arg
	thunk \args
	pushq \arg
	.endif
.endm

.macro thunkto reg:req,args:vararg
	thunk \args
	mov %rsp, \reg
.endm

.macro blackhole
	mov (%rbp), %rax
	mov %rax, 010(%rbp)
	movq $BLE_code, (%rbp)
.endm

.endif # _macros_s_file
