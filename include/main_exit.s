.ifndef _main_exit_s_file
_main_exit_s_file:

# exitcode -> ||
.thunkcode main_exit
	mov 010(%rsi), %rdi # result INT goes to syscall exitcode
	mov $60, %rax  # exit=60
	syscall # exit %rdi
	# syscall might also die; at that point let's die more elaborately
	mov 0, %rax
	jmp main_exit

.endif # _main_exit_s_file
