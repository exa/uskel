
.ifndef _intops_s_file
_intops_s_file:

.include "include/primops.s"

.primop2 plus
	mov 010(%rsi), %rax # arg 2
	mov 020(%rbp), %rsi # location of arg1
	add 010(%rsi), %rax # arg 1
	primop2_ret_int %rax

.primop2 mul
	mov 010(%rsi), %rax # arg 2
	mov 020(%rbp), %rsi # location of arg1
	mulq 010(%rsi) # arg 1 (goes to %rax and %rdx)
	primop2_ret_int %rax

.primop2 sub
	mov 020(%rbp), %rdi # location of arg1
	mov 010(%rdx), %rax # arg 1
	sub 010(%rsi), %rax # arg 2
	primop2_ret_int %rax

.endif # _intops_s_file
