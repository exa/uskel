
# uskel runtime and start; include this at the top.

.section .init
.global _start
_start:
	jmp _uskel_start

.include "include/macros.s"

.section .bss
_unix_rsp:
	# back-up of program entry rsp (aka the actual stack given by the
	# actual OS; we might like to use it at some point, maybe)
	cell 0

.include "include/gc.s"

.section .text

_uskel_start:
	# we use the stack pointer for easy writing to the heap;
	# back it up to memory state just if we ever needed it again.
	mov %rsp, _unix_rsp

	# allocate the initial chunk of memory
	mov $_uskel_start_main, %rsi
	jmp _uskel_gc_init

	_uskel_start_main:
	# push a thunk for main
	pushq $0
	pushq $main

	# loop the continuation to itself (prevents gc trouble, should never be reached)
	mov %rsp, %rsi
	enter %rsp # run the program
	# Q: are there gonna be functions that have both the argument AND the cont?
	# A: No, stuff is either entered as return-continuation (takes res,
	# cont has to be saved) or as forward call (takes cont)
	#
	# (A needs validation)
