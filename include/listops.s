
.ifndef _listops_s_file
_listops_s_file:

.include "include/primops.s"

# | n | list | -> cont
.primop2 list_int_index
	needs_alloc $060

	mov 010(%rsi), %rdx # the list constructor id, must be 1
	cmp $1, %rdx
	jne list_int_index_not_found

	mov 020(%rbp), %rcx
	mov 010(%rcx), %rcx
	test %rcx, %rcx
	jz list_int_index_found # we are taking the head, all happy, return it

	# more likely, we need to continue -- make new args thunks
	sub $1, %rcx
	thunkto %r11, $INT_code, %rcx # decreased index
	thunkto %r11, $list_int_index, $2, %r11, 030(%rsi) # new call on tail

	primop2_cont_indirect %r11

list_int_index_found:
	mov 020(%rsi), %rax #head
	primop2_cont_indirect %rax

list_int_index_not_found:
	movq 0, %rax #fault (TODO: we should have a better fault)

.endif # _listops_s_file
