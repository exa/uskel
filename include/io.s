
.ifndef _io_s_file
_io_s_file:

# | int | -> cont
.thunkcode print
	needs_alloc $040
	thunkto %rsi, $print_fini, $2, %rbp, %rsi
	enter 020(%rbp)

# arg -> | ret | cont |
.thunkcode print_fini
	needs_alloc $0110 #64 bit characters + 8 backup
	mov 010(%rsi), %rax

	# make a string
	mov %rsp, %r15
	sub $1, %r15
	movb $0x0a, (%r15)
	print_fini_loop:
	mov %al, %bl
	and $1, %bl
	add $0x30, %bl
	sub $1, %r15
	movb %bl, (%r15)
	shr $1, %rax
	jnz print_fini_loop

	mov $1, %rdi #stdout
	mov %rsp, %rdx
	sub %r15, %rdx #size
	mov %r15, %rsi #buf
	mov $1, %rax #write
	syscall

	mov 020(%rbp), %rsi
	movq $0, 010(%rsi)
	movq $INT_code, 0(%rsi)
	enter 030(%rbp)

.endif # _io_s_file
