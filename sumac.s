

.include "include/uskel.s"

.include "include/data.s"
.include "include/io.s"
.include "include/intops.s"

.primop2 sumac
	needs_alloc $0100

	mov 020(%rbp), %rdi #1st arg
	mov 010(%rdi), %rcx #1st arg val
	mov 010(%rsi), %rax #2nd arg val

	cmp $0, %rcx
	jz sumac_ret

	add %rcx, %rax
	dec %rcx
	thunkto %r10, $INT_code, %rcx
	thunkto %r11, $INT_code, %rax
	thunkto %r10, $sumac, $2, %r10, %r11
	primop2_cont_indirect %r10

	sumac_ret:
	primop2_ret_int %rax

.thunkcode main
	needs_alloc $0150

	thunkto %r11, $INT_code, $10000000
	thunkto %r12, $INT_code, $0
	thunkto %r11, $sumac, $2, %r11, %r12

	thunkto %r11, $print, $1, %r11
	thunkto %rsi, $main_exit, $0
	enter %r11

.include "include/main_exit.s"
