
.include "include/uskel.s"

.include "include/apply.s"
.include "include/intops.s"
.include "include/io.s"
.include "include/main_exit.s"

.thunkcode main
	# make an integer
	thunkto %r11, $INT_code, $1

	# make an empty closure for adding stuff
	thunkto %r12, $FUN2_code, $plus, $0

	# apply first argument
	thunkto %r12, $apply, $2, %r12, %r11

	# apply second argument (the p.a. function part is still in r12)
	thunkto %r12, $apply, $2, %r12, %r11

	# print the result
	thunkto %r12, $print, $1, %r12

	# make a continuation for main (exit) and set it for print call
	thunkto %rsi, $main_exit, $0

	# start evaluating the print
	enter %r12
