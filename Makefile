
SYMSRCS=$(wildcard *.s)
OBJS=$(SYMSRCS:.s=.o)
PROGS=$(SYMSRCS:.s=)

.SUFFIXES:

all: $(PROGS)

clean:
	rm -f $(OBJS) $(PROGS)

%.o: %.s $(wildcard include/*.s)
	as -g $< -o $@

%: %.o
	ld $@.o -o $@
