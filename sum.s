

.include "include/uskel.s"

.include "include/data.s"
.include "include/io.s"
.include "include/intops.s"

.primop1 sumn
	needs_alloc $0110

	mov 010(%rsi), %rax
	test %rax, %rax
	jz sumn_zero

	dec %rax
	thunkto %r10, $INT_code, %rax
	thunkto %r10, $sumn, $1, %r10
	thunkto %r10, $plus, $2, %rsi, %r10 #TODO try the other way?
	primop1_cont_indirect %r10

	sumn_zero:
	primop1_ret_int $0

# || -> cont
.thunkcode main
	needs_alloc $0160
	# push a new integer
	thunkto %r11, $INT_code, $10000000

	# push the plus
	thunkto %r11, $sumn, $1, %r11

	# push the print
	thunkto %r11, $print, $1, %r11

	# push a cont thunk for main_exit
	thunkto %rsi, $main_exit, $0

	# evaluate into main_exit
	enter %r11

.include "include/main_exit.s"
