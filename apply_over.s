

.include "include/uskel.s"

.include "include/apply.s"
.include "include/intops.s"
.include "include/io.s"
.include "include/main_exit.s"

# | int | int | -> int
.thunkcode const_int
	mov %rsi, %rax
	mov 020(%rbp), %rsi
	enter %rax

# | int | int | -> int
.thunkcode tsnoc_int
	mov %rsi, %rax
	mov 030(%rbp), %rsi
	enter %rax

# | int | -> (| int | int | -> int)
.primop1 if_int
	mov 010(%rsi), %rax
	mov $const_int, %r10
	mov $tsnoc_int, %r11
	test %rax, %rax
	cmovz %r11, %r10
	thunkto %r10, $FUN2_code, %r10, $0
	primop1_cont_indirect %r10

.thunkcode main
	# make integers
	thunkto %r10, $INT_code, $0
	thunkto %r11, $INT_code, $1
	thunkto %r12, $INT_code, $2

	# make the function
	thunkto %r13, $FUN1_code, $if_int, $0

	# send enough arguments
	thunkto %r13, $apply, $4, %r13, %r10, %r11, %r12

	# print the result
	thunkto %r13, $print, $1, %r13

	# make a continuation for main (exit) and set it for print call
	thunkto %rsi, $main_exit, $0

	# start evaluating the print
	enter %r13
