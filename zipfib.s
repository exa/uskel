
.include "include/uskel.s"

.include "include/listops.s"
.include "include/intops.s"
.include "include/io.s"
.include "include/main_exit.s"
.include "include/apply.s"

.thunkcode zipWith
	needs_alloc $070
	thunkto %rsi, $zipWith_arg1, $5, 020(%rbp), 030(%rbp), 040(%rbp), %rbp, %rsi
	blackhole
	enter 030(%rbp)

.thunkcode zipWith_arg1
	movq $zipWith_fini, (%rbp)
	mov %rsi, 030(%rbp)
	mov %rbp, %rsi
	mov 040(%rbp), %rbp
	enter_rbp

.thunkcode zipWith_fini
	needs_alloc $0150
	mov 030(%rbp), %r8 # arg1
	mov %rsi, %r9 # arg2
	cmpq $0, 010(%r8)
	je zipWith_null
	cmpq $0, 010(%r9)
	je zipWith_null

	# f (head arg1) (head arg2) : zipWith f (tail arg1) (tail arg2)
	thunkto %r10, $zipWith, $3, 020(%rbp), 030(%r8), 030(%r9)
	thunkto %r11, $apply, $3, 020(%rbp), 020(%r8), 020(%r9)
	thunkto %rsi, $LIST_code, $1, %r11, %r10

	zipWith_ret:
	mov 050(%rbp), %r8
	movq $IND_code, 000(%r8)
	mov %rsi, 010(%r8)
	mov 060(%rbp), %rbp
	enter_rbp

	zipWith_null:
	thunkto %rsi, $LIST_code, $0
	jmp zipWith_ret


.thunkcode main
	needs_alloc $0370
	# x = 0 : 1 : zipWith plus x (tail x)
	thunkto %r8, $FUN2_code, $plus, $0
	thunkto %r8, $zipWith, $3, %r8, $0, $0
	thunkto %r9, $INT_code, $1
	thunkto %r9, $LIST_code, $1, %r9, %r8
	thunkto %r10, $INT_code, $0
	thunkto %r10, $LIST_code, $1, %r10, %r9
	# recurse args!
	mov %r10, 030(%r8)
	mov %r9, 040(%r8)

	thunkto %r8, $INT_code, $25
	thunkto %r8, $list_int_index, $2, %r8, %r10
	thunkto %r8, $print, $1, %r8
	thunkto %rsi, $main_exit, $0
	enter %r8
