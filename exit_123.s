
.include "include/uskel.s"

.include "include/data.s"

# || -> cont
.thunkcode main
	needs_alloc $0120
	thunkto %r11, $INT_code, $100
	thunkto %r12, $INT_code, $23
	thunkto %r13, $plus, $2, %r11, %r12
	thunkto %rsi, $main_exit, $1
	enter %r13

.include "include/main_exit.s"

.include "include/intops.s"
