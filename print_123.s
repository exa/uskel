
.include "include/uskel.s"

.include "include/data.s"
.include "include/io.s"
.include "include/intops.s"

# || -> cont
.thunkcode main
	needs_alloc $0160
	# push a new integer
	thunkto %r11, $INT_code, $100

	# push another new integer
	thunkto %r12, $INT_code, $23

	# push the plus
	thunkto %r11, $plus, $2, %r11, %r12

	# push the print
	thunkto %r11, $print, $1, %r11

	# push a cont thunk for main_exit and set continuation for main_exit
	thunkto %rsi, $main_exit, $0

	# evaluate into main_exit
	enter %r11

.include "include/main_exit.s"
