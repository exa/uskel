
.include "include/uskel.s"

.include "include/listops.s"
.include "include/intops.s"
.include "include/io.s"
.include "include/main_exit.s"

# | lag1 | lag2 | -> cont
.thunkcode fibs
	needs_alloc $0140
	# next value
	thunkto %r11, $plus, $2, 020(%rbp), 030(%rbp)

	# fib call with the next value
	thunkto %r12, $fibs, $2, 030(%rbp), %r11

	# cons list with the lagged arg
	thunkto %r13, $LIST_code, $1, 020(%rbp), %r12

	# TODO simplify
	# replace self with IND
	mov %r13, 010(%rbp)
	movq $IND_code, 0(%rbp)

	mov %rsi, %rbp
	mov %r13, %rsi
	enter_rbp

# || -> cont
.thunkcode fibs0
	needs_alloc $0100
	thunkto %r12, $INT_code, $1
	thunkto %r11, $INT_code, $0
	thunk $fibs, $2, %r11, %r12
	enter %rsp

.thunkcode main
	needs_alloc $0160
	thunkto %r12, $fibs0, $0
	thunkto %r11, $INT_code, $20
	thunkto %r11, $list_int_index, $2, %r11, %r12
	thunkto %r11, $print, $1, %r11
	thunkto %rsi, $main_exit, $0
	enter %r11
